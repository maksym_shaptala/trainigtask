package trainingtask

import groovy.transform.ToString

import java.time.LocalDateTime

@ToString(includes = 'text')
class Message {

    String text
    LocalDateTime published
    LocalDateTime modified

    static belongsTo = [user: User]

    def beforeInsert() {
        published = modified = LocalDateTime.now()
    }

    def beforeUpdate() {
        modified = LocalDateTime.now()
    }

    static constraints = {
        text blank: false, maxSize: 280, widget: 'textarea'
        published nullable: true
        modified nullable: true
        user nullable: true
    }

    static mapping = {
        id column: 'MessageID'
        user column: 'UserID'
        sort published: 'desc'
    }
}
