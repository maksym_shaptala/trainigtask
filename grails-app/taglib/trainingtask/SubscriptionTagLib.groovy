package trainingtask

class SubscriptionTagLib {
    static defaultEncodeAs = [taglib:'raw']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def springSecurityService

    static namespace = "subscription"

    def isSubscribe = { attrs, body ->
        def currentUser = springSecurityService.getCurrentUser() as User
        if(currentUser?.subscriptions?.contains(attrs?.user)) {
            out << body()
        }
    }

    def isNotSubscribe = { attrs, body ->
        def currentUser = springSecurityService.getCurrentUser() as User
        if(!currentUser?.subscriptions?.contains(attrs?.user)) {
            out << body()
        }
    }
}
