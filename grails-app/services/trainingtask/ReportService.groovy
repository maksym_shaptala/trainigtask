package trainingtask


import grails.transaction.Transactional
import grails.util.Holders
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource
import net.sf.jasperreports.engine.util.JRLoader
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.core.io.Resource

@Transactional
class ReportService {

    @Value('${jasper.dir.reports}')
    String jasperDir

    @Value('${jasper.filename}')
    String jasperFilename

    private static ApplicationContext getApplicationContext() {
        Holders.grailsApplication.mainContext
    }

    @Transactional(readOnly = true)
    def exportMessagesToPdf(User user, Map params) {
        Resource jasper = getApplicationContext().getResource("${jasperDir}/${jasperFilename}.jasper")
        def jasperReport// = JasperCompileManager.compileReport(file.getAbsolutePath())
        if(jasper.exists()) {
            log.info("Load ${jasper.file.absolutePath} file")
            jasperReport = JRLoader.loadObject(jasper.file) as JasperReport
        } else {
            Resource jrxml = getApplicationContext().getResource("${jasperDir}/${jasperFilename}.jrxml")
            if(jrxml.exists()) {
                log.info("Load ${jrxml.file.absolutePath} file")
                jasperReport = JasperCompileManager.compileReport(jrxml.file.getAbsolutePath())
            }
            else
            {
                throw new RuntimeException('Report file not found')
            }
        }

        def messages = Message.findAllByUser(user, params).collect({[text: it.text, published: it.published.toDate()]})
        def parameters = [title: user.username,
                          description: 'Count: ' + messages.size()]
        def dataSource = new JRBeanCollectionDataSource(messages)
        def jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource)
        JasperExportManager.exportReportToPdf(jasperPrint)
    }
}
