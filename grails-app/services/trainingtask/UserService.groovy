package trainingtask

import grails.transaction.Transactional

@Transactional
class UserService {

    def springSecurityService

    def save(User user) {
        if (user == null || user.hasErrors()) {
            transactionStatus.setRollbackOnly()
            return
        }

        user.save flush: true
        UserRole.create(user, Role.findByAuthority("ROLE_USER"), true)
    }

    def subscribe(User user) {
        def currentUser = springSecurityService.getCurrentUser()
        if(currentUser == null || user == null) {
            transactionalStatus.setRollBackOnly()
            return
        }

        currentUser.addToSubscriptions(user)
        currentUser.save flash: true
    }

    def unsubscribe(User user) {
        def currentUser = springSecurityService.getCurrentUser()
        if(currentUser == null || user == null) {
            transactionalStatus.setRollBackOnly()
            return
        }

        currentUser.removeFromSubscriptions(user)
        currentUser.save flash: true
    }
}
