package trainingtask

import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.security.access.prepost.PostAuthorize
import org.springframework.security.access.prepost.PostFilter
import org.springframework.security.access.prepost.PreAuthorize


@Transactional(readOnly = true)
class MessageService {

    def springSecurityService

    @PostFilter('filterObject.user.username == principal.username')
    def findAllMessages(GrailsParameterMap params) {
        Message.findAllByUser(springSecurityService.getCurrentUser(), params)
    }

    @PostAuthorize("returnObject.user.username == principal.username")
    def getMessage(Message message) {
        message
    }

    @Transactional
    @PreAuthorize("#message.user.username == principal.username")
    def deleteMessage(Message message) {
        if (message == null) {
            transactionStatus.setRollbackOnly()
            return
        }
        message.delete flush: true
    }

    @Transactional
    def saveMessage(Message message) {
        if (message == null || message.hasErrors()) {
            transactionStatus.setRollbackOnly()
        }
        message.user = springSecurityService.getCurrentUser()
        message.save flush: true
    }

    @Transactional
    @PreAuthorize("#message.user.username == principal.username")
    def updateMessage(Message message) {
        if (message == null || message.hasErrors()) {
            transactionStatus.setRollbackOnly()
            return
        }
        message.save flush: true
    }
}
