<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>
    <g:layoutHead/>
</head>

<body>
<header>
    <div id="grailsLogo" role="banner">
        <a href="/"><asset:image class="round" src="twitter.png" alt="Twitter"/>
            <span>Twitter</span>
        </a>
    </div>

</header>

<nav class="nav">
    <ul>
        <sec:ifLoggedIn>
            <li>
                <g:link controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}">
                    Welcome <sec:username/>
                </g:link>
            </li>
            <li>
                <g:link controller="message">Your messages</g:link>
            </li>
        </sec:ifLoggedIn>
        <sec:ifAllGranted roles="ROLE_ADMIN">
            <li><g:link controller="info" action="index">App Info</g:link></li>
            <li><g:link controller="role" action="index">Roles</g:link></li>
            <li><a href="/dbconsole">H2 Console</a></li>
        </sec:ifAllGranted>
        <li><g:link controller="user">Users</g:link></li>
        <sec:ifNotLoggedIn>
            <li><g:link controller="login">Sing in</g:link></li>
            <li><g:link controller="user" action="singUp">Sing up</g:link></li>
        </sec:ifNotLoggedIn>
        <sec:ifLoggedIn>
            <li><a href="#logout" id="logout">Logout</a></li>
            <script>
                $("#logout").click(function (e) {
                    e.preventDefault();
                    $.post("/logout/index")
                        .always(function () {
                            window.location = "/";
                        });
                });
            </script>
        </sec:ifLoggedIn>
    </ul>
</nav>
<g:layoutBody/>
<div class="footer" role="contentinfo"></div>

<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
</body>
</html>
