<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>

<div id="show-user" class="content scaffold-show" role="main">
    <h1><g:message code="default.user_info.label"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}.
        Redirect to login page <span id="timer">3</span>
        </div>
        <script>
            const timer = $("#timer");
            setTimeout(function () {
                window.location = "/login";
            }, 4000);
            setInterval(function () {
                timer.text(parseInt(timer.text()) - 1);
            }, 1000);
        </script>
    </g:if>
    <div>
        <p>
            <span class="property-label">Username</span>

            <span class="property-value" aria-labelledby="user-name-label">
                ${user.username}
            </span>
        </li>
    </div>
    <g:if test="${user?.messages?.size() == 0}">
        <p>User ${user.username} didn't publish any message yet</p>
    </g:if>
    <g:else>
        <sec:ifLoggedIn>
            <g:if test="${sec.loggedInUserInfo(field: 'username') != user.username}">
                <div>
                    <subscription:isSubscribe user="${user}">
                        <p>You followed this user</p>
                        <g:link controller="user" action="unsubscribe" id="${user.id}"
                                class="subscribe">Unsubscribe</g:link>
                    </subscription:isSubscribe>
                    <subscription:isNotSubscribe user="${user}">
                        <g:link controller="user" action="subscribe" id="${user.id}"
                                class="subscribe">Subscribe to ${user.username}</g:link>
                    </subscription:isNotSubscribe>
                </div>
            </g:if>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <p>You can't follow this user because you should
            <g:link controller="login">
                Sing In
            </g:link>
            </p>
        </sec:ifNotLoggedIn>

        <table>
            <thead>
            <tr>
                <th>Message</th>
                <th>Published</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${user.messages}">
                <tr>
                    <td>${it.text}</td>
                    <td width="15%">
                        <g:formatDate style="SHORT" type="datetime" date="${it.published.toDate()}"/>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
        <div><g:link controller="report" class="subscribe" action="forUser" id="${user.id}">Download messages</g:link></div>
    </g:else>
</div>
</body>
</html>
