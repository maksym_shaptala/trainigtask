<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.singup.label"/></title>
</head>

<body>

<div id="create-user" class="content scaffold-create" role="main">
%{--    <h1><g:message code="default.singup.label"/></h1>--}%
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.user}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <div id="singin">
        <div class="inner">
            <div class="fheader">Please <g:message code="default.singup.label"/></div>
            <g:form url="[action: 'save']" class="cssform">
                <p>
                    <label for="username">Username<span class="required-indicator">*</span>
                    </label>
                    <g:field type="text" class="text_" required="" name="username" id="username"/>
                </p>

                <p>
                    <label for="password">Password<span class="required-indicator">*</span>
                    </label>
                    <g:field class="text_" type="password" required="" name="password" id="password"/>
                </p>

                <p>
                    <g:submitButton name="submit"
                                    value="${message(code: 'default.button.singup.label', default: 'Sing Up')}"/>
                </p>
            </g:form>
        </div>
    </div>
</div>
</body>
</html>
