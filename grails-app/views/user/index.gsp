<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div id="list-user" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol>
        <g:each in="${userList}">
            <li><g:link action="show" id="${it.id}">${it.username}</g:link></li>
        </g:each>
    </ol>

    %{--            <div class="pagination">--}%
    %{--                <g:paginate total="${userCount ?: 0}" />--}%
    %{--            </div>--}%
</div>
</body>
</html>