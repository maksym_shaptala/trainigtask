<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>

<body>

<div id="page-body" class="content" role="main">

    <sec:ifNotLoggedIn>
        <h1>Welcome to Twitter Clone</h1>

        <p>
            You can read messages all users. To do it fallow this link <g:link controller="user">Users</g:link></p>

        <p>
            To post your own message you should have registration. Please follow to this link
            <g:link controller="user" action="singUp">Sing Up</g:link>
        </p>

        <p>
            If you have been registered follow this link
            <g:link controller="login">Sing In</g:link>
            to enter in your account
        </p>
    </sec:ifNotLoggedIn>
    <sec:ifLoggedIn>
        <g:if test="${messages?.size() == 0}">
            <h1>You don't have any subscription to any User</h1>

            <p>You can subscribe to follow post other users</p>
        </g:if>
        <g:else>
            <h1>Your subscription</h1>
            <table>
                <thead>
                <tr>
                    <th>Message</th>
                    <th>User</th>
                    <th>Published</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${messages}">
                    <tr>
                        <td>${it.text}</td>
                        <td>${it.user.username}</td>
                        <td><g:formatDate date="${it.published.toDate()}" type="datetime" style="SORT"/></td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </g:else>
    </sec:ifLoggedIn>
</div>
</body>
</html>