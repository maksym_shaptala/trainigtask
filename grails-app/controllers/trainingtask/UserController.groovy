package trainingtask


import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NOT_FOUND

class UserController {

    static allowedMethods = [save: "POST"]

    def userService

    @Transactional(readOnly = true)
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond User.list(params), model: [userCount: User.count()]
    }

    def show(User user) {
        respond user
    }

    def singUp() {
        respond new User(params)
    }

    def save(User user) {
        if (user == null) {
            notFound()
            return
        }

        if (user.hasErrors()) {
            respond user.errors, view: 'singUp'
            return
        }

        userService.save(user)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.registered.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect user
            }
            '*' { respond user, [status: CREATED] }
        }
    }

    def subscribe(User user) {
        if (user == null || user.hasErrors()) {
            redirect(action: 'index')
            return
        }

        userService.subscribe(user)
        redirect(action: 'show', id: user.id)
    }

    def unsubscribe(User user) {
        if (user == null || user.hasErrors()) {
            redirect(action: 'index')
            return
        }

        userService.unsubscribe(user)
        redirect(action: 'show', id: user.id)
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
