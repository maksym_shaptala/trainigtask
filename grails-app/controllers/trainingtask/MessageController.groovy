package trainingtask

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Secured('ROLE_USER')
class MessageController {

    def messageService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def messages = messageService.findAllMessages(params)
        respond messages, model: [messageCount: messages.size()]
    }

    def show(Message message) {
        respond messageService.getMessage(message)
    }

    def create() {
        respond new Message(params)
    }

    def save(Message msg) {
        if (msg == null) {
            notFound()
            return
        }

        if (msg.hasErrors()) {
            respond msg.errors, view: 'create'
            return
        }
        messageService.saveMessage(msg)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'msg.label', default: 'Message'), msg.id])
                redirect msg
            }
            '*' { respond msg, [status: CREATED] }
        }
    }

    def edit(Message message) {
        respond messageService.getMessage(message)
    }

    def update(Message msg) {
        if (msg == null) {
            notFound()
            return
        }

        if (msg.hasErrors()) {
            respond msg.errors, view: 'edit'
            return
        }

        messageService.updateMessage(msg)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.msg', args: [message(code: 'msg.label', default: 'Message'), msg.id])
                redirect msg
            }
            '*' { respond msg, [status: OK] }
        }
    }

    def delete(Message msg) {

        if (msg == null) {
            notFound()
            return
        }

        messageService.deleteMessage(msg)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'msg.label', default: 'Message'), msg.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'message.label', default: 'Message'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
