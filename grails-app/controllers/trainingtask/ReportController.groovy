package trainingtask

class ReportController {

    def reportService

    def forUser(User user) {
        if (user == null) {
            redirect(controller: 'main')
            return
        }

        response.setHeader("Content-disposition", "attachment; filename=${user.username}.pdf")
        response.contentType = "application/pdf"
        def pdf = reportService.exportMessagesToPdf(user, params)
        response.contentLengthLong = pdf.length
        response.outputStream.write pdf
    }
}
