package trainingtask

class MainController {

    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def u = springSecurityService.currentUser as User
        def messages = ((u?.subscriptions as List<User>)*.messages as List)?.flatten()
        [messages: messages]
    }
}
