package trainingtask

import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class InfoController {

    def index() {}
}
