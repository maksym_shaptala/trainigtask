package trainingtask

import java.time.LocalDateTime

class LocalDateTimeExtension {
    static Date toDate(final LocalDateTime self) {
        new Date(self.getYear(), self.getMonthValue(), self.getDayOfMonth(),
                self.getHour(), self.getMinute(), self.getSecond())
    }
}
