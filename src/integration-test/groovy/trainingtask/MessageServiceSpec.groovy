package trainingtask

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.access.AccessDeniedException
import spock.lang.Specification

@Integration
@Rollback
class MessageServiceSpec extends Specification {

    SpringSecurityService springSecurityService
    MessageService messageService
    AuthenticationManager authenticationManager

    def logIn(String login, String password) {
        def adminAuth = new UsernamePasswordAuthenticationToken(login, password)
        def auth = authenticationManager.authenticate(adminAuth)
        SecurityContextHolder.getContext().setAuthentication(auth)
    }

    def logOut() {
        SecurityContextHolder.clearContext()
    }

    def setup() {

    }

    def cleanup() {
    }

    void 'Test admin authentication'() {
        given:
        logIn(login, password)
        when:
        def user = springSecurityService.getCurrentUser()
        then:
        springSecurityService.loggedIn
        user.username == login
        where:
        login   | password
        'admin' | 'qwerty'
        'max'   | 'max'
    }

    void "Test logIn and logOut"() {
        when:
        logIn('admin', 'qwerty')
        and:
        logOut()
        then:
        !springSecurityService.loggedIn
    }

    void "Test save message for current user"() {
        given:
        logIn(login, password)
        def message = new Message(text: 'Hello World')
        when:
        messageService.saveMessage(message)
        then:
        User.findByUsername(login).messages == [message] as Set<Message>
        where:
        login   | password
        'admin' | 'qwerty'
        'max'   | 'max'
    }

    void "Test find messages for current user"() {
        given:
        logIn(login, password)
        def messages = [new Message(text: 'Hello'), new Message(text: 'World')]
        messages.each { messageService.saveMessage(it) }
        when:
        def foundMessages = messageService.findAllMessages()
        then:
        Message.count == 2
        foundMessages as Set<Message> == messages as Set<Message>
        where:
        login   | password
        'admin' | 'qwerty'
        'max'   | 'max'
    }

    void "Test find only messages for current user"() {
        given:
        logIn(login1, password1)
        def messages = [new Message(text: 'Hello'), new Message(text: 'World')]
        messages.each { messageService.saveMessage(it) }
        and:
        logOut()
        logIn(login2, password2)
        messageService.saveMessage(new Message(text: 'Other message'))
        and:
        logOut()
        logIn(login1, password1)
        when:
        def foundMessages = messageService.findAllMessages()
        then:
        Message.count == 3
        foundMessages as Set<Message> == messages as Set<Message>
        where:
        login1  | password1 | login2  | password2
        'admin' | 'qwerty'  | 'max'   | 'max'
        'max'   | 'max'     | 'admin' | 'qwerty'
    }

    void "Test method getMessage with message belongs to current user"() {
        given: 'Login as user1'
        logIn(login1, password1)
        and: 'Save message1'
        def message1 = new Message(text: 'Hello')
        messageService.saveMessage(message1)
        and: 'Logout'
        logOut()
        and: 'Login as user2'
        logIn(login2, password2)
        and: 'Save message2'
        def message2 = new Message(text: 'Other message')
        messageService.saveMessage(message2)
        and: 'Logout'
        logOut()
        and: 'Login as user1 again'
        logIn(login1, password1)
        when: 'Get message witch belongs currentUser (user1)'
        def message = messageService.getMessage(message1)
        then: 'We should get origin message1'
        message1 == message
        where:
        login1  | password1 | login2  | password2
        'admin' | 'qwerty'  | 'max'   | 'max'
        'max'   | 'max'     | 'admin' | 'qwerty'
    }

    void "Test method getMessage with message belongs to other user"() {
        given: 'Login as user1'
        logIn(login1, password1)
        and: 'Save message1'
        def message1 = new Message(text: 'Hello')
        messageService.saveMessage(message1)
        and: 'Logout'
        logOut()
        and: 'Login as user2'
        logIn(login2, password2)
        and: 'Save message2'
        def message2 = new Message(text: 'Other message')
        messageService.saveMessage(message2)
        and: 'Logout'
        logOut()
        and: 'Login as user1 again'
        logIn(login1, password1)
        when: "Get message witch doesn't belongs currentUser (user1)"
        messageService.getMessage(message2)
        then: 'Should catch Access denied'
        thrown AccessDeniedException
        where:
        login1  | password1 | login2  | password2
        'admin' | 'qwerty'  | 'max'   | 'max'
        'max'   | 'max'     | 'admin' | 'qwerty'
    }

    void "Test for delete own message for current user"() {
        given: 'Login as user'
        logIn(login, password)
        and: 'Save two messages'
        def message1 = new Message(text: 'Hello')
        messageService.saveMessage(message1)
        def message2 = new Message(text: 'World')
        messageService.saveMessage(message2)
        and: 'Logout'
        logOut()
        and: 'Login as user'
        logIn(login, password)
        when: "Delete message1"
        messageService.deleteMessage(message1)
        then: 'Left only one message'
        Message.count == 1
        messageService.findAllMessages() == [message2]
        where:
        login   | password
        'admin' | 'qwerty'
        'max'   | 'max'
    }

    void "Test for AccessDeniedException while delete message for other user"() {
        given: 'Login as user1'
        logIn(login1, password1)
        and: 'Save one messages'
        def message = new Message(text: 'Hello')
        messageService.saveMessage(message)
        and: 'Logout'
        logOut()
        and: 'Login as user2'
        logIn(login2, password2)
        when: "Try to delete user1"
        messageService.deleteMessage(message)
        then: 'Should get AccessDeniedException'
        thrown AccessDeniedException
        where:
        login1  | password1 | login2  | password2
        'admin' | 'qwerty'  | 'max'   | 'max'
        'max'   | 'max'     | 'admin' | 'qwerty'
    }

    void "Test for update own message for current user"() {
        given: 'Login as user'
        logIn(login, password)
        and: 'Save two messages'
        def message = new Message(text: 'Hello')
        messageService.saveMessage(message)
        and: 'Logout'
        logOut()
        and: 'Login as user'
        logIn(login, password)
        when: "Update message"
        message = messageService.getMessage(message)
        message.text = 'World'
        messageService.updateMessage(message)
        then: 'Message should be updated'
        Message.count == 1
        messageService.findAllMessages() == [message]
        where:
        login   | password
        'admin' | 'qwerty'
        'max'   | 'max'
    }

    // TODO: update message for other user
    void "Test for AccessDeniedException while update message for other user"() {
        given: 'Login as user1'
        logIn(login1, password1)
        and: 'Save one messages'
        def message = new Message(text: 'Hello')
        messageService.saveMessage(message)
        and: 'Logout'
        logOut()
        and: 'Login as user2'
        logIn(login2, password2)
        when: "Try to update user1"
        message.text = 'Hack'
        messageService.updateMessage(message)
        then: 'Should get AccessDeniedException'
        thrown AccessDeniedException
        where:
        login1  | password1 | login2  | password2
        'admin' | 'qwerty'  | 'max'   | 'max'
        'max'   | 'max'     | 'admin' | 'qwerty'
    }
}
