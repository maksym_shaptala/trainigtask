package trainingtask

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(Message)
class MessageSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll('validate on a message with text #text should have returned #shouldBeValid')
    void "test text valid"() {
        expect:
            new Message(text: text).validate(['text']) == shouldBeValid
        where:
        text    |   shouldBeValid
        'hello' |   true
        ''      |   false
        null    |   false
        'a'*280 |   true
        'a'*281 |   false
    }
}