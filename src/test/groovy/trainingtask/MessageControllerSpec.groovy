package trainingtask

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.*
import spock.lang.*

@TestFor(MessageController)
@Mock([Message, MessageService, SpringSecurityService])
class MessageControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        params.text = 'Hello World!'
    }

    void "Test the index action returns the correct model"() {
        when: "The index action is executed"
        controller.index()

        then: "The model is correct"
        !model.messageList
        model.messageCount == 0
    }

    void "Test the create action returns the correct model"() {
        when: "The create action is executed"
        controller.create()

        then: "The model is correctly created"
        model.message != null
    }

    void "Test the save action correctly return create view for invalid message"() {
        when: "The save action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def message = new Message()
        message.validate()
        controller.save(message)

        then: "The create view is rendered again with the correct model"
        model.message != null
        view == 'create'
    }

    void "Test the save action correctly persists an instance"() {
        when: "The save action is executed with a valid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        populateValidParams(params)
        controller.save()

        then: "A redirect is issued to the show action"
        response.redirectedUrl == '/message/show/1'
        controller.flash.message != null
        Message.count() == 1
    }

    void "Test that the show action returns 404 with a null domain"() {
        when: "The show action is executed with a null domain"
        controller.show(null)

        then: "A 404 error is returned"
        response.status == 404
    }

    void "Test that the show action returns the correct model"() {
        when: "A domain instance is passed to the show action"
        populateValidParams(params)
        def message = new Message(params)
        controller.show(message)

        then: "A model is populated containing the domain instance"
        model.message == message
    }

    void "Test that the edit action returns 404 with a null message"() {
        when: "The edit action is executed with a null domain"
        controller.edit(null)

        then: "A 404 error is returned"
        response.status == 404
    }

    void "Test that the edit action returns the correct model"() {
        when: "A domain instance is passed to the edit action"
        populateValidParams(params)
        def message = new Message(params)
        controller.edit(message)

        then: "A model is populated containing the domain instance"
        model.message == message
    }

    void "Test the update action redirect to /message/index for a null domain instance"() {
        when: "Update is called for a domain instance that doesn't exist"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        controller.update(null)

        then: "Redirect to /message/index"
        response.redirectedUrl == '/message/index'
        flash.message != null
    }

    void "Test the update action for invalid domain instance"() {
        when: "An invalid domain instance is passed to the update action"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        def message = new Message()
        message.validate()
        controller.update(message)

        then: "The edit view is rendered again with the invalid instance"
        view == 'edit'
        model.message == message
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when: "A valid domain instance is passed to the update action"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        populateValidParams(params)
        def message = new Message(params).save(flush: true)
        controller.update(message)

        then: "A redirect is issued to the show action"
        message != null
        response.redirectedUrl == "/message/show/$message.id"
        flash.message != null
    }

    void "Test that the delete redirect to /message/index for null instance"() {
        when: "The delete action is called for a null instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(null)

        then: "Redirect to /message/index"
        response.redirectedUrl == '/message/index'
        flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        given:
        def message = new Message (params).save(flush: true)

        when: "The domain instance is passed to the delete action"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(message)

        then: "The instance is deleted"
        Message.count() == 0
        response.redirectedUrl == '/message/index'
        flash.message != null
    }
}
