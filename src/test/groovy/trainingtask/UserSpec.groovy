package trainingtask

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(User)
class UserSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll('validate on a user with username #username should have returned #shouldBeValid')
    void "test username validation"() {
        expect:
            new User(username: username).validate(['username']) == shouldBeValid
        where:
        username    |   shouldBeValid
        'admin'     |   true
        ''          |   false
        null        |   false
    }

    @Unroll('validate on a user with username #password should have returned #shouldBeValid')
    void "test password validation"() {
        expect:
        new User(password: password).validate(['password']) == shouldBeValid
        where:
        password    |   shouldBeValid
        'admin'     |   true
        ''          |   false
        null        |   false
    }
}