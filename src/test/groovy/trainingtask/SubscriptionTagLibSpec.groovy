package trainingtask

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.TestFor
import spock.lang.Shared
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(SubscriptionTagLib)
class SubscriptionTagLibSpec extends Specification {
    @Shared def follower
    @Shared def user

    def setup() {
        follower = new User(username: 'amdin') as User
        def message = new Message(text: 'Hello')
        user = new User(username: 'max', messages: [message] as Set<Message>)
    }

    def cleanup() {
    }

    void "test isSubscribe() for user has subscriptions"() {
        given:
        follower.subscriptions = [user] as Set<User>
        tagLib.springSecurityService = Stub(SpringSecurityService) {
            getCurrentUser() >> follower
        }
        when:
        def template = '<subscription:isSubscribe user="${user}">yes</subscription:isSubscribe>'
        then:
        applyTemplate(template, [user: user]) == 'yes'
    }

    void "test isNotSubscribe() for user has not subscriptions fot this user"() {
        given:
        follower.subscriptions = []
        tagLib.springSecurityService = Stub(SpringSecurityService) {
            getCurrentUser() >> follower
        }
        when:
        def template = '<subscription:isNotSubscribe user="${user}">yes</subscription:isNotSubscribe>'
        then:
        applyTemplate(template, [user: user]) == 'yes'
    }

    void "test isNotSubscribe() for user has subscriptions fot this user"() {
        given:
        follower.subscriptions = [user] as Set<User>
        tagLib.springSecurityService = Stub(SpringSecurityService) {
            getCurrentUser() >> follower
        }
        when:
        def template = '<subscription:isNotSubscribe user="${user}">yes</subscription:isNotSubscribe>'
        then:
        applyTemplate(template, [user: user]) == ''
    }

    void "test isSubscribe() for user don't have subscriptions"() {
        given:
        follower.subscriptions = []
        tagLib.springSecurityService = Stub(SpringSecurityService) {
            getCurrentUser() >> follower
        }
        when:
        def template = '<subscription:isSubscribe user="${user}">yes</subscription:isSubscribe>'
        then:
        applyTemplate(template, [user: user]) == ''
    }

    void "test isNotSubscribe() for user don't have subscriptions"() {
        given:
        follower.subscriptions = []
        tagLib.springSecurityService = Stub(SpringSecurityService) {
            getCurrentUser() >> follower
        }
        when:
        def template = '<subscription:isNotSubscribe user="${user}">yes</subscription:isNotSubscribe>'
        then:
        applyTemplate(template, [user: user]) == 'yes'
    }
}
