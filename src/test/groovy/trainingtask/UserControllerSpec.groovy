package trainingtask

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.web.ControllerUnitTestMixin
import spock.lang.Specification

@TestFor(UserController)
@TestMixin(ControllerUnitTestMixin)
@Mock([User, UserService, Role, UserRole, SpringSecurityService])
class UserControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        params['username'] = 'admin'
        params['password'] = 'admin'
    }

    void "Test the index action returns the correct model"() {
        when: "The index action is executed"
        controller.index()

        then: "The model is correct"
        !model.userList
        model.userCount == 0
    }

    void "Test the singUp action returns the correct model"() {
        given:
        params['username'] = 'admin'

        when: "The singUp action is executed"
        controller.singUp()

        then: "The model is correctly created"
        model.user != null
        model.user.username == 'admin'
    }

    void "Test the save action correctly render again for invalid user instance"() {
        when: "The save action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def user = new User()
        user.validate()
        controller.save(user)

        then: "The singUp view is rendered again with the correct model"
        model.user != null
        view == 'singUp'
    }

    void "Test the save action correctly persists an instance"() {
        given:
        populateValidParams(params)
        def user = new User(params)

        when: "The save action is executed with a valid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        controller.save(user)

        then: "A redirect is issued to the show action"
        response.redirectedUrl == '/user/show/1'
        controller.flash.message != null
        User.count() == 1
    }

    void "Test that the show action returns 404 with a null argument"() {
        when: "The show action is executed with a null domain"
        controller.show(null)

        then: "A 404 error is returned"
        response.status == 404
    }

    void "Test that the show action returns the correct model"() {
        when: "A domain instance is passed to the show action"
        populateValidParams(params)
        def user = new User(params)
        controller.show(user)

        then: "A model is populated containing the domain instance"
        model.user == user
    }

    void "Test than the subscribe action redirect to /user/index with a null domain"() {
        when: "The subscribe action is executed with a null domain"
        controller.subscribe(null)

        then: "Expected redirect to action index"
        response.redirectedUrl == '/user/index'
    }

    void "Test than the subscribe action make correct redirection"() {
        given:
        new User(username: 'admin', password: 'qwerty').save(validate: false)
        controller.userService.springSecurityService = Stub(SpringSecurityService) {
            getCurrentUser() >> new User()
        }

        when: "A domain instance is passed to the subscribe action"
        response.reset()
        params.id = 1
        controller.subscribe()

        then: 'Expected redirect to a /user/show/1'
        response.redirectedUrl == '/user/show/1'
    }

    void "Test than the unsubscribe action to /user/index with a null domain"() {
        when: "The unsubscribe action is executed with a null domain"
        controller.unsubscribe(null)

        then: "Expected redirect to action index"
        response.redirectedUrl == '/user/index'
    }
    void "Test than the unsubscribe action make correct redirection"() {
        given:
        new User(username: 'admin', password: 'qwerty').save(validate: false)
        controller.userService.springSecurityService = Stub(SpringSecurityService) {
            getCurrentUser() >> new User()
        }

        when: "A domain instance is passed to the unsubscribe action"
        response.reset()
        params.id = 1
        controller.unsubscribe()

        then: 'Expected redirect to a /user/show/1'
        response.redirectedUrl == '/user/show/1'
    }
}
