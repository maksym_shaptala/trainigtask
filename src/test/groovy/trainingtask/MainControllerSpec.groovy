package trainingtask

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(MainController)
class MainControllerSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test invoke index() as anonymous"() {
        given:
        controller.springSecurityService = Mock(SpringSecurityService)
        when:
        final res = controller.index()
        then:
        res.messages == null
    }

    void "test invoke index() as user"() {
        given:
        def user = new User(username: 'amdin') as User
        def message = new Message(text: 'Hello')
        user.subscriptions = [new User(username: 'max', messages: [message] as Set<Message>)] as Set<User>
        controller.springSecurityService = Stub(SpringSecurityService) {
            getCurrentUser() >> user
        }
        when:
        final res = controller.index()
        then:
        res.messages == [message]
    }
}
