package trainingtask

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification

import java.time.LocalDateTime

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class LocalDateExtensionSpec extends Specification {


    void "test convert LocalDateTime to Date"() {
        given:"LocalDateTime"
            final localDateTime = LocalDateTime.parse("2021-08-03T15:30:00")
        expect:"Convert to date by method toDate()"
            localDateTime.toDate() == new Date(2021, 8, 3, 15, 30)
    }
}