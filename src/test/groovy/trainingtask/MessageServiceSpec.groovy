package trainingtask

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(MessageService)
@Mock([Message, User])
class MessageServiceSpec extends Specification {

    def setup() {
        def admin = new User(username: 'amdin', password: 'test').save()
        service.springSecurityService = Stub(SpringSecurityService) {
            getCurrentUser() >> admin
        }
    }

    def cleanup() {
    }

    def "Test save domain instance"() {
        given:
        def msg = new Message(text: 'Hello World')

        when:
        service.saveMessage(msg)

        then:
        Message.count == 1
        Message.get(1) == msg
    }

    def "Test find all messages for current user"() {
        given:
        def message = new Message(text: 'Hello World')
        service.saveMessage(message)

        when:
        def messages = service.findAllMessages()

        then:
        messages.size() == 1
        messages == [message]
    }

    def "Test delete message"() {
        given:
        def message = new Message(text: 'Hello World').save()

        when:
        service.deleteMessage(message)

        then:
        Message.count == 0
    }

    def "Test update message"() {
        given:
        def message = new Message(text: 'Hello').save()

        when:
        message.text = 'Hello World'
        service.updateMessage(message)

        then:
        Message.count == 1
        Message.get(1).text == 'Hello World'
    }

}
