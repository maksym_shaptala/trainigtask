package trainingtask

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(UserService)
@Mock([UserRole, Role, User])
class UserServiceSpec extends Specification {

    def setup() {
        new Role(authority: 'ROLE_USER').save()
    }

    def cleanup() {
    }

    void "Test save domain instance"() {
        given:
        def user = new User(username: 'admin', password: 'qwerty')

        when:
        service.save(user)

        then:
        User.count() == 1
        User.get(1) == user
    }

    void "Test subscription"() {
        given:
        def admin = new User(username: 'admin', password: 'qwerty').save()
        def user = new User(username: 'user', password: 'user').save()

        service.springSecurityService = Stub(SpringSecurityService){
            getCurrentUser() >> admin
        }

        when:
        service.subscribe(user)

        then:
        admin.subscriptions.size() == 1
        admin.subscriptions == [user] as Set<User>
    }

    void "Test unsubscription"() {
        given:
        def admin = new User(username: 'admin', password: 'qwerty').save() as User
        def user = new User(username: 'user', password: 'user').save()
        service.springSecurityService = Stub(SpringSecurityService){
            getCurrentUser() >> admin
        }
        admin.addToSubscriptions(user)

        when:
        service.unsubscribe(user)

        then:
        admin.subscriptions.size() == 0
    }
}
