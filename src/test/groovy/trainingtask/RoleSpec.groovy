package trainingtask

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(Role)
class RoleSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll('validate on a role with #authority should have returned #shouldBeValid')
    void "test authority validation"() {
        expect:
        new Role(authority: authority).validate(['authority']) == shouldBeValid
        where:
        authority    | shouldBeValid
        'ROLE_ADMIN' | true
        ''           | false
        null         | false

    }
}